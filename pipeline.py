#!/usr/bin/env python3 -W ignore::DeprecationWarning
# -*- coding: utf-8 -*-
"""
Created on Sat Dec  1 07:41:40 2018

@author: leon
"""
import warnings
warnings.filterwarnings("ignore")

import argparse
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Activation
from keras.layers import Conv2D, MaxPooling2D
from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
from keras import optimizers
from keras.models import load_model
import csv
from skimage.io import imread, imsave

import numpy as np
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import sys
import h5py
from tqdm import tqdm



LEFT_BOUND = -3;
RIGHT_BOUND = 2;
LOWER_BOUND = 0;
UPPER_BOUND = 25;

MAP_HEIGHT = 50
MAP_WIDTH = 100

parser = argparse.ArgumentParser()

parser.add_argument('path_to_file', type=str)
parser.add_argument('static_or_dynamic', type=int) # 0: Static, 1: Dynamic

args = parser.parse_args()


if not os.path.exists("buffer/"):
    os.makedirs("buffer/")
    
path = ""


print(""" _ .-') _     ('-.     ('-.     _ (`-.                                        ('-.              
( (  OO) )  _(  OO)  _(  OO)   ( (OO  )                                      ( OO ).-.          
 \     .'_ (,------.(,------. _.`     \   ,------. .-'),-----.   ,----.      / . --. /  ,-.-')  
 ,`'--..._) |  .---' |  .---'(__...--''('-| _.---'( OO'  .-.  ' '  .-./-')   | \-.  \   |  |OO) 
 |  |  \  ' |  |     |  |     |  /  | |(OO|(_\    /   |  | |  | |  |_( O- ).-'-'  |  |  |  |  \ 
 |  |   ' |(|  '--. (|  '--.  |  |_.' |/  |  '--. \_) |  |\|  | |  | .--, \ \| |_.'  |  |  |(_/ 
 |  |   / : |  .--'  |  .--'  |  .___.'\_)|  .--'   \ |  | |  |(|  | '. (_/  |  .-.  | ,|  |_.' 
 |  '--'  / |  `---. |  `---. |  |       \|  |_)     `'  '-'  ' |  '--'  |   |  | |  |(_|  |    
 `-------'  `------' `------' `--'        `--'         `-----'   `------'    `--' `--'  `--'    """)

print("Preprocessing Sensor Data" )

if 'sensorX' in h5py.File(args.path_to_file, mode="r").keys():
    hf = h5py.File(args.path_to_file, 'r')
    data_sx = hf.get('sensorX').value
    data_sy = hf.get('sensorY').value
    data_sz = hf.get('sensorZ').value
    intensity = hf.get('intensity').value
    distance = hf.get('distance').value
    valid_mask = hf.get('pointValid').value.astype(np.bool)
    
    LEFT_BOUND = -3
    RIGHT_BOUND = 2
    LOWER_BOUND = 0
    UPPER_BOUND = 25
    
    file = open("buffer/" + "cloud" + ".xyz", "w")
    path = file
    
    for i in tqdm(range(0, len(data_sx))):
        for j in range(0, len(data_sx[i])):
            if valid_mask[i][j].astype(int) and data_sx[i][j] > LEFT_BOUND and data_sx[i][j] < RIGHT_BOUND and data_sy[i][j] > LOWER_BOUND and data_sy[i][j] < UPPER_BOUND:
                file.write(str(data_sx[i][j]) + " " + str(data_sy[i][j]) + " " + str(data_sz[i][j]) + " " + str(intensity[i][j]) + " " + str(distance[i][j]) +"\n")
    file.close()

print("Done.\n")

density_map = np.zeros((MAP_HEIGHT + 1, MAP_WIDTH + 1), dtype=np.int32)
intensity_map = np.zeros((MAP_HEIGHT + 1, MAP_WIDTH + 1), dtype=np.float32)

max_intensity = 0
max_density = 0

print("Generating Heatmap")

with open("buffer/cloud.xyz") as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=' ')
    line_count = 0
    for row in csv_reader:
        x = (float(row[0]) - LEFT_BOUND) / (RIGHT_BOUND - LEFT_BOUND) #normalise x
        y = (float(row[1]) - LOWER_BOUND) / (UPPER_BOUND - LOWER_BOUND) #normalise y
        z = float(row[2])
        intensity = float(row[3])
        #distance = row[4]
        line_count += 1
        
        #print("x: " + str(x) + ", y: " + str(y));
        
        density_map[int(x*MAP_HEIGHT),int(y*MAP_WIDTH)] += 1
        intensity_map[int(x*MAP_HEIGHT), int(y*MAP_WIDTH)] += intensity

print("Done.\n")
#density_map /= line_count
intensity_map /= line_count


#plt.imshow(density_map)
#plt.show()

imsave("buffer/map.png", density_map)

print("Classifying\n")

classes = ['Fog - Visibility less than 20m', 'Fog - Visibility less than 30m', 'Fog - Visibility less than 40m', 'Fog - Visibility less than 50m', 'Clear - Visibility more than 50m']
model = load_model('my_model_dyn_dyn_high_90.h5')
if(args.static_or_dynamic == 0):
    model = load_model('my_model_stat_stat.h5')
    classes = ['Fog - Visibility less than 20m', 'Fog - Visibility less than 30m', 'Fog - Visibility less than 40m', 'Fog - Visibility less than 50m', 'Fog - Visibility less than 65m', 'Clear - Visibility more than 65m']

    
img = image.load_img('buffer/map.png', target_size=(101, 51))
x = image.img_to_array(img)
x = np.expand_dims(x, axis=0)

images = np.vstack([x])
prediction = model.predict_classes(images, batch_size=10)

print(classes[int(prediction[0])])

print("\n")