# DEEPFOG:AI

DEEPFOG:AI is able to detect fog in scenerios based on [HDF5-Files](https://en.wikipedia.org/wiki/Hierarchical_Data_Format).

## Before using

Before using DEEPFOG:AI you need to install several packages. 

- [argparse](https://pypi.org/project/argparse/)

- [keras](https://pypi.org/project/Keras/)

- [tensorflow](https://pypi.org/project/Keras/)

- [csv](https://pypi.org/project/csv/)

- [h5py](https://pypi.org/project/h5py/)

- [numpy](https://pypi.org/project/numpy/)


## Usage

```
python pypline.py [file] [dynamic]

```

Arguments:
- [file] Path + Filename
- [dynamic] 0 for static and 1 for dynamic



