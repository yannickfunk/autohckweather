import cv2
import csv
import numpy as np
from matplotlib import pyplot as plt

filename = "fog40m-50m_146.xyz"

array = np.zeros((60, 260), dtype=np.uint8)

with open(filename) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=' ')
    for row in csv_reader:
        if (float(row[2]) > -2 and float(row[2]) < -1):
            x = np.round((float(row[0]) + 3) * 10)
            y = np.round(float(row[1]) * 10)
            array[int(x),int(y)] = 255



kernel = np.ones((3,3),np.uint8)
dilation = cv2.dilate(array,kernel,iterations = 1)

kernel = np.ones((3,3),np.uint8)
erosion = cv2.erode(dilation,kernel,iterations = 1)

plt.subplot(121),plt.imshow(array,cmap = 'gray')
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(erosion,cmap = 'gray')
plt.title('Edge Image'), plt.xticks([]), plt.yticks([])

plt.show()