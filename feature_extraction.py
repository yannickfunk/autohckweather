import os
import sys
import numpy as np
import csv
import matplotlib.pyplot as plt

dir = "/media/andre/Volume/AUTOnmsHCK/result2"

array = np.zeros((60, 260), dtype=np.uint8)
file = open("label.xyz", "w")
file.write("label" + "," + "bucket1" + "," + "bucket2" + "," + "bucket3\n")

for filename in tqdm(os.listdir(dir)):
    label = "fog50m-60m"
    with open(dir+"\\"+filename) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=' ')
        for row in csv_reader:
            if (float(row[2]) > -2 and float(row[2]) < -1):
                x = np.round((float(row[0]) + 3) * 10)
                y = np.round(float(row[1]) * 10)
                array[int(x),int(y)] = 255    
        summed = [0,0,0]
        for k in range(6):
            i = 5 + k * 10
            for j in range(40, 100):
                if array[i, j - 1] < array[i, j]:
                    summed[0] += 1
            for j in range(100, 165):
                if array[i, j - 1] < array[i, j]:
                    summed[1] += 1
            for j in range(165, 260):
                if array[i, j - 1] < array[i, j]:
                    summed[2] += 1

    file.write(label + ","  + str(summed[0]) + "," + str(summed[1]) + "," + str(summed[2]) +"\n")
file.close()

            
