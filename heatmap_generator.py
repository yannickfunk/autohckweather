#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 30 20:24:33 2018

@author: leon
"""

import csv
import numpy as np 
from skimage.io import imread, imsave
import matplotlib.pyplot as plt 
import os
import concurrent.futures

LEFT_BOUND = -3;
RIGHT_BOUND = 2;
LOWER_BOUND = 0;
UPPER_BOUND = 25;

MAP_HEIGHT = 50
MAP_WIDTH = 100

IMAGE_THRESHOLD = 1;

data_directory = "result3" # <- replace ...

def calcHeatmap(filename):
    if filename.endswith(".xyz"):
        print("opened " + filename)
        density_map = np.zeros((MAP_HEIGHT + 1, MAP_WIDTH + 1), dtype=np.int32)
        intensity_map = np.zeros((MAP_HEIGHT + 1, MAP_WIDTH + 1), dtype=np.float32)
        
        max_intensity = 0
        max_density = 0
        
        try:
            with open(data_directory + "/" + filename) as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=' ')
                line_count = 0
                for row in csv_reader:
                    x = (float(row[0]) - LEFT_BOUND) / (RIGHT_BOUND - LEFT_BOUND) #normalise x
                    y = (float(row[1]) - LOWER_BOUND) / (UPPER_BOUND - LOWER_BOUND) #normalise y
                    z = float(row[2])
                    intensity = float(row[3])
                    #distance = row[4]
                    line_count += 1
                    
                    #print("x: " + str(x) + ", y: " + str(y));
                    
                    density_map[int(x*MAP_HEIGHT),int(y*MAP_WIDTH)] += 1
                    intensity_map[int(x*MAP_HEIGHT), int(y*MAP_WIDTH)] += intensity
            
            
            #density_map /= line_count
            intensity_map /= line_count
            
            
            #plt.imshow(density_map)
            #plt.show()
            if not os.path.exists("dense_map/" + filename[0:5]):
                os.makedirs("dense_map/" + filename[0:5])
                
            if not os.path.exists("intensity_map/" + filename[0:5]):
                os.makedirs("intensity_map/" + filename[0:5])
            
            
            
            imsave("dense_map/" + filename[0:5] + "/" + filename + ".png", density_map)
            imsave("intensity_map/" + filename[0:5] + "/" + filename + ".png", intensity_map)
        except:
            print("Error");
            



if not data_directory:
    print("Please set data_directory to publish the file contents!")
    sys.exit(0)


#for filename in os.listdir(data_directory + "/"):
    #if filename.endswith(".xyz"):
        
with concurrent.futures.ProcessPoolExecutor() as executor:
   executor.map(calcHeatmap, os.listdir(data_directory + "/"))
