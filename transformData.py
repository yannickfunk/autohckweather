#!/usr/bin/env python3

"""
Warning: Ros Docu server was unreachable when this snippet was written
I apologize for any inconviences. Good luck!
"""

import os
import sys
import numpy as np
import h5py

import matplotlib.pyplot as plt

data_directory = ".../day1" # <- replace ...

if not data_directory:
    print("Please set data_directory to publish the file contents!")
    sys.exit(0)

for dir in os.listdir(data_directory):
    id = 1
    for filename in os.listdir(data_directory + "/" + dir):
        if filename.endswith(".hdf5"):
            print(filename)
            filepath = os.path.join(data_directory + "/" + dir, filename)
            if 'sensorX' in h5py.File(os.path.join(data_directory + "/" + dir, filename), mode="r").keys():
                hf = h5py.File(filepath, 'r')
                data_sx = hf.get('sensorX').value
                data_sy = hf.get('sensorY').value
                data_sz = hf.get('sensorZ').value
                intensity = hf.get('intensity').value
                distance = hf.get('distance').value
                valid_mask = hf.get('pointValid').value.astype(np.bool)

                LEFT_BOUND = -3
                RIGHT_BOUND = 2
                LOWER_BOUND = 0
                UPPER_BOUND = 25

                file = open("result/" + dir + "_" + str(id) + ".xyz", "w")
                id += 1

                for i in range(0, len(data_sx)):
                    for j in range(0, len(data_sx[i])):
                        if valid_mask[i][j].astype(int) and data_sx[i][j] > LEFT_BOUND and data_sx[i][j] < RIGHT_BOUND and data_sy[i][j] > LOWER_BOUND and data_sy[i][j] < UPPER_BOUND:
                            file.write(str(data_sx[i][j]) + " " + str(data_sy[i][j]) + " " + str(data_sz[i][j]) + " " + str(intensity[i][j]) + " " + str(distance[i][j]) +"\n")
                file.close()
