from sklearn.svm import SVC
from sklearn.model_selection import cross_validate


import pandas as pd

label = pd.read_csv("label.csv", delimiter=";")
y = label["label"]
X = label.drop("label", axis=1)
svc = SVC()
a = cross_validate(svc, X=X, y=y, cv=5)
print(a)


